import java.io.File
import javax.imageio.ImageIO


import org.scalatra._
import java.net.URL
import org.scalatra.scalate.ScalateSupport

class MapMakerAPI extends ScalatraServlet with ScalateSupport {

  get("/") {
    val mapMakerService = new MapMakerService()
    mapMakerService.parseHeirarchy()
    mapMakerService.createThreeSignalBitmap("Z638", "CTS3617")
    val error = 0

    <html>
      <h1>Hello, world! Error = {error}</h1>
      <a href="woof">Try an SSP Page</a>
    </html>
  }

  get("/woof") {
    contentType="text/html"
    ssp("woof.ssp","date" -> new java.util.Date)
  }

  case class Flower(slug: String, name: String) {
    def toXML= <flower name={name}>{slug}</flower>
  }

  val all = List(
    Flower("yellow-tulip", "Yellow Tulip"),
    Flower("red-rose", "Red & Rose"),
    Flower("black-rose", "Black Rose"))

  get("/flowers"){
    contentType="text/xml"
    <flowers>
      { all.map(_.toXML) }
    </flowers>
  }

}
