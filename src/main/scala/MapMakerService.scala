import java.awt.Color
import java.awt.image.BufferedImage
import java.io.File
import java.nio.charset.CodingErrorAction
import javax.imageio.ImageIO
import com.vividsolutions.jts.geom.{Coordinate, GeometryFactory, Geometry}

import scala.collection.mutable
import scala.io.{Codec, Source}

/**
  * Created by Hunter on 1/25/2016.
  */
class MapMakerService {
  case class Kit(latLon: Option[(Float, Float)],
                 countryName: String,
                 isNumerator: Boolean)

  case class BoundingBox(minLat: Float,
                         maxLat: Float,
                         minLon: Float,
                         maxLon: Float)

  val europeBounds = BoundingBox(30.0F, 75.0F, -15.0F, 60.0F)
  val rows = 500
  val cols = 500

  val geometries = new mutable.HashMap[String, Geometry]()
  val countryRegions = new mutable.HashMap[String, Set[String]]
  val countryCellWeights = new mutable.HashMap[String, Map[(Int, Int), Float]]
  val countryCertaintyWeight = new mutable.HashMap[String, Float]
  val geomFactory = new GeometryFactory()
  val worldValidity = new mutable.HashSet[(Int, Int)]

  def loadGeometries() {
    Source.fromFile("src\\main\\resources\\geometries").getLines().foreach(l => {
      val countrySplit = l.split("=")
      val countryName = countrySplit(0)
      val pointsSplit = countrySplit(1).split(" ")
      val polygon = geomFactory.createPolygon(pointsSplit.map(p => {
        val lonLatSplit = p.split(",")
        new Coordinate(lonLatSplit(0).toDouble, lonLatSplit(1).toDouble)
      }))
      geometries.put(countryName, polygon)
    })
    Source.fromFile("src\\main\\resources\\countryRegions").getLines().foreach(l => {
      val countryRegionSplit = l.split("=")
      val countryName = countryRegionSplit(0)
      countryRegions.put(countryName, countryRegionSplit(1).split(",").toSet)
    })
    geometries.keySet.foreach(regionName => {
      turnCountryIntoGridWeights(europeBounds, regionName)._1.foreach(g => worldValidity.add(g._1))
    })
  }

  def getCellDims(boundingBox: BoundingBox, cols: Int, rows: Int): (Float, Float) = {
    val cellWidth = (boundingBox.maxLon - boundingBox.minLon) / cols
    val cellHeight = (boundingBox.maxLat - boundingBox.minLat) / rows
    (cellWidth, cellHeight)
  }

  def turnCountryIntoGridWeights(boundingBox: BoundingBox, countryName: String): (Map[(Int, Int), Float], Float) = {
    val weightedCells = mutable.HashMap[(Int, Int), Float]()

    val countryGeoms = countryRegions.get(countryName) match {
      case Some(x) => x.map(regionName => geometries.get(regionName).get) ++ (if (geometries.get(countryName).isDefined) Set(geometries.get(countryName).get) else Set())
      case _ => Set(geometries.get(countryName).get)
    }

    val (cellWidth, cellHeight) = getCellDims(boundingBox, cols, rows)
    //println(cellWidth + " " + cellHeight)
    var row = 0
    var totalArea = 0F
    while (row < rows) {
      var col = 0
      val centerLat = boundingBox.minLat + (row.toFloat + 0.5F) * cellHeight
      val areaScalars = Math.cos(Math.PI / 180 * centerLat).toFloat
      while (col < cols) {

        val thisPoint = geomFactory.createPoint(new Coordinate(boundingBox.minLon + (col.toFloat + 0.5F) * cellWidth,
          centerLat))
        if (countryGeoms.exists(g => g.contains(thisPoint))) {
          totalArea = totalArea + areaScalars
          weightedCells.put((col, row), areaScalars)
        }
        col = col + 1
      }
      row = row + 1
    }
    val areaOfThisInTermsOfCircles = Math.max(totalArea * 111.2 * 111.321 * cellWidth * cellHeight / Math.PI / 160 / 160, 1)

    val certaintyWeight = (1F / (areaOfThisInTermsOfCircles * 3 * Math.sqrt(areaOfThisInTermsOfCircles))).toFloat

    weightedCells.foreach{case((col: Int, row: Int), area: Float) => {

      weightedCells.put((col: Int, row: Int), area / totalArea)
    }}

    (weightedCells.toMap, certaintyWeight)
  }

  def distanceFromLatLonToGridCenter(boundingBox: BoundingBox, lat: Float, lon: Float, centerLat: Float, centerLon: Float): Float = {
    val yDisplacementKm = (centerLat - lat) * 111.321
    val latOfMidpoint = (centerLat + lat) / 2
    val xDisplacementKm =  (centerLon - lon) * 111.321 * Math.cos(Math.PI / 180 * latOfMidpoint)
    Math.sqrt(yDisplacementKm * yDisplacementKm + xDisplacementKm * xDisplacementKm).toFloat
  }

  def turnLatLonIntoGridWeights(boundingBox: BoundingBox, lat: Float, lon: Float, radius: Float): Map[(Int, Int), (Float, Float)] = {
    val weightedCells = mutable.HashMap[(Int, Int), (Float, Float)]()
    val (cellWidth, cellHeight) = getCellDims(boundingBox, cols, rows)
    var row = 0
    var totalArea = 0F
    var weightSum = 0F
    while (row < rows) {
      var col = 0
      val centerLat = boundingBox.minLat + (row.toFloat + 0.5F) * cellHeight
      val area = Math.cos(Math.PI / 180 * centerLat).toFloat
      while (col < cols) {
        val centerLon = boundingBox.minLon + (col.toFloat + 0.5F) * cellWidth
        val dist = distanceFromLatLonToGridCenter(boundingBox, lat, lon, centerLat, centerLon)

        if (dist < radius) {
          val radMinusDistOverRad = (radius - dist) / radius
          val thisWeight = radMinusDistOverRad * area * 3 / radius / Math.PI.toFloat

          weightedCells.put((col, row), (thisWeight, radMinusDistOverRad))
        }
        col = col + 1
      }
      row = row + 1
    }
    //println("area sum = " + totalArea + ", weightsum = " + weightSum)
    //println("max weight point data to a grid cell = " + weightedCells.map(_._2).max)
    weightedCells.toMap
  }

  val heirarchy = mutable.Map[String, String]()

  def parseHeirarchy(): Unit = {
    Source.fromFile("C:\\Users\\Hunter\\Downloads\\codes.csv").getLines().foreach(l => {
      val split = l.split(",")
      heirarchy.put(split(0), split(1))
    })
    println(heirarchy.toMap.mkString(","))
    println("Z631 " + downstream("Z631", Set()))
    println("Z2444 " + downstream("Z2444", Set()))
  }

  def downstream(clade: String, downstreamClades: Set[String]): Set[String] = {
    val children = heirarchy.keys.filter(k => heirarchy.get(k).get == clade)
    return downstreamClades ++ children ++ children.flatMap(c => downstream(c, Set()))
  }

  val signal = "Z8425"
  lazy val signalParent = heirarchy.get(signal).get
  //val secondSignal = "Z1295"

  //signal vs upstream by verified not downstream of signal
  def parseCSV(): (Map[String, (Int, Int)],
                   Seq[(Float, Float, Boolean)]) = {
    val countryData = mutable.HashMap[String, (Int, Int)]()
    var pointData = scala.collection.mutable.ListBuffer[(Float, Float, Boolean)]()
    val decoder = Codec.UTF8.decoder.onMalformedInput(CodingErrorAction.IGNORE)
    var skip = true
    var unknownLocations = mutable.HashMap[String, Int]()
    var linesSeen = 0
    var denoms = 0
    var uniqueDenomClassifications = mutable.HashSet[String]()

    val downstreamSignal = downstream(signal, Set())
    val downstreamParent = downstream(signalParent, Set())


    Source.fromFile("C:\\Users\\Hunter\\Downloads\\kits.txt")(decoder).getLines().foreach(l => {
      if (!skip) {
        linesSeen = linesSeen + 1
        val kitData = l.split("\t")
        val subGroup = kitData(3)
        val negatives = kitData(4).split(",")
        println(subGroup)

        val isNumerator = subGroup == signal || downstreamSignal.contains(subGroup)
        val isDenominator = isNumerator || (subGroup == signalParent && negatives.contains(signal)) || downstreamParent.contains(subGroup)
        val countryName = kitData(7)
        val lat = kitData(11).toFloat
        val lon = kitData(12).toFloat
        if (isDenominator) {
          if (!isNumerator) uniqueDenomClassifications.add(subGroup)
          denoms = denoms + 1
          if (!lat.equals(0.0F) || !lon.equals(0.0F)) {
            pointData += ((lat, lon, isNumerator))
            //println(lat + " " + lon)
          } else if (geometries.keySet.contains(countryName) || countryRegions.keySet.contains(countryName)) {
            val existing = countryData.getOrElse(countryName, (0, 0))
            countryData.put(countryName, (existing._1 + (if(isNumerator) 1 else 0), existing._2 + 1))
            //println(countryName)
          } else {
            unknownLocations.put(countryName, unknownLocations.getOrElse(countryName, 0) + 1)
          }
        }
      } else {
        skip = false
      }
    })
    println(uniqueDenomClassifications.size + " unique denominators")
    uniqueDenomClassifications.foreach(println)
    println(linesSeen + " lines processed")
    val countries = countryData.toSeq.map(p => p._2._2).sum
    println(denoms + " matching haplogroup kits")
    println((countries + pointData.size) + " haplogroup matching kits with valid location")
    println(countries + " country kits RIGHT")
    println(countryData.map(p => p._2._2).sum + " country kits WRONG")
    println(pointData.size + " point kits")
    println(unknownLocations.toSeq.map(unk => unk._2).sum + " unidentified regions")
    unknownLocations.foreach(unk => println(unk._1 + " " + unk._2))
    (countryData.toMap, pointData.toSeq)
  }

  def parseCSVTwoSignals(signalOne: String, signalTwo: String): (Map[String, Array[Int]],
    Seq[(Float, Float, Int)]) = {
    val countryData = mutable.HashMap[String, Array[Int]]()
    var pointData = scala.collection.mutable.ListBuffer[(Float, Float, Int)]()
    val decoder = Codec.UTF8.decoder.onMalformedInput(CodingErrorAction.IGNORE)
    var skip = true
    var unknownLocations = mutable.HashMap[String, Int]()
    var linesSeen = 0
    var denoms = 0
    var uniqueDenomClassifications = mutable.HashSet[String]()
    val parent = heirarchy.get(signalOne).get
    if (heirarchy.get(signalOne).get != heirarchy.get(signalTwo).get) throw new IllegalArgumentException("must be sibling clades")

    val downstreamSignalOne = downstream(signalOne, Set())
    val downstreamSignalTwo = downstream(signalTwo, Set())
    val downstreamParent = downstream(parent, Set())

    Source.fromFile("C:\\Users\\Hunter\\Downloads\\kitsfakebasal6.txt")(decoder).getLines().foreach(l => {
      if (!skip) {
        linesSeen = linesSeen + 1
        val kitData = l.split("\t")
        val subGroup = kitData(3)
        val negatives = kitData(4).split(",")
        println(subGroup)

        val isSignalOne = subGroup == signalOne || downstreamSignalOne.contains(subGroup)
        val isSignalTwo = subGroup == signalTwo || downstreamSignalTwo.contains(subGroup)
        val isBasal = (!isSignalOne && !isSignalTwo) && (
          ((subGroup == parent) && negatives.toSeq.contains(signalOne) && negatives.toSeq.contains(signalTwo)) ||
            downstreamParent.contains(subGroup)
          )
        println("negatives contains " + signalOne + ", " + negatives.toSeq.contains(signalOne))
        println("negatives contains " + signalTwo + ", " + negatives.toSeq.contains(signalTwo))
        println("subgroup == parent " + subGroup + ", " + parent + ": " + (subGroup == parent))
        if (isBasal) println("basal found " + kitData(0))
        if (!isBasal) println(kitData(0) + " is not basal " + isSignalOne + isSignalTwo + " negatives: " + negatives.toSeq.mkString(",") + " subgroup " + subGroup + negatives.toSeq.contains(signalOne) + negatives.toSeq.contains(signalTwo) + parent + signalOne + signalTwo + " negatives " + negatives.size)
        val countryName = kitData(7)
        val lat = kitData(11).toFloat
        val lon = kitData(12).toFloat

        if (isSignalOne || isSignalTwo || isBasal) {
          val signalIndex = if (isSignalOne) 0 else if (isSignalTwo) 1 else if (isBasal) 2 else -1
          denoms = denoms + 1
          if (!lat.equals(0.0F) || !lon.equals(0.0F)) {
            pointData += ((lat, lon, signalIndex))
            //println(lat + " " + lon)
          } else if (geometries.keySet.contains(countryName) || countryRegions.keySet.contains(countryName)) {
            val existing = countryData.getOrElse(countryName, Array[Int](0,0,0))
            existing(signalIndex) = existing(signalIndex) + 1
            countryData.put(countryName, existing)
            //println(countryName)
          } else {
            unknownLocations.put(countryName, unknownLocations.getOrElse(countryName, 0) + 1)
          }
        }
      } else {
        skip = false
      }
    })
    println(linesSeen + " lines processed")
    val countries = countryData.toSeq.map(p => p._2.sum).sum
    println(denoms + " matching haplogroup kits")
    println((countries + pointData.size) + " haplogroup matching kits with valid location")
    println(countries + " country kits")
    println(pointData.size + " point kits")
    println(unknownLocations.toSeq.map(unk => unk._2).sum + " unidentified regions")
    unknownLocations.foreach(unk => println(unk._1 + " " + unk._2))
    (countryData.toMap, pointData.toSeq)
  }


  def createBitmap() {
    if (geometries.keySet.size == 0) loadGeometries()

    /*val countryNumDenoms = Map[String, (Int, Int)]("Germany" -> (0,10),
                                                   "France" -> (1,1))
*/
    val numDenoms = mutable.HashMap[(Int, Int), (Float, Float, Float)]()

    def addToExistingNumDenoms(col: Int, row: Int, weight: Float, numerator: Int, denominator: Int, certaintyWeight: Float) {
      numDenoms.get(col, row) match {
        case Some(x) => numDenoms.put((col, row), (x._1 + weight * numerator, x._2 + weight * denominator, x._3 + certaintyWeight))
        case _ => numDenoms.put((col, row), (weight * numerator, weight * denominator, certaintyWeight))
      }
    }

    def processCountry(countryName: String, numerator: Int, denominator: Int): Unit = {
      println("processing " + countryName + " into grid cells")
      if (countryCellWeights.get(countryName).isEmpty) {
        val (gridWeights, certaintyWeight) = turnCountryIntoGridWeights(europeBounds, countryName)
        countryCellWeights.put(countryName, gridWeights)
        countryCertaintyWeight.put(countryName, certaintyWeight)
      }
      val cWeight = countryCertaintyWeight.get(countryName).get * denominator
      countryCellWeights.get(countryName).get.foreach{case((col: Int, row: Int), weight: Float) => addToExistingNumDenoms(col, row, weight, numerator, denominator, cWeight)
      }
    }
    println("parsing CSV")
    val (countryNumDenoms, pointNumerators) = parseCSV()

    countryNumDenoms.foreach{case((countryName: String, (numerator: Int, denominator: Int))) => processCountry(countryName, numerator, denominator)}

    /*val pointNumerators = Seq[(Float, Float, Boolean)]((48.333F, 11.566F, true))*/

    pointNumerators.foreach{case((lat: Float, lon: Float, isNumerator: Boolean)) => {
      turnLatLonIntoGridWeights(europeBounds, lat, lon, 160F).foreach{case((col: Int, row: Int), (weight: Float, certaintyWeight: Float)) => {
        if (worldValidity.contains((col, row))) {
          addToExistingNumDenoms(col, row, weight, if (isNumerator) 1 else 0, 1, certaintyWeight)
        }
    }}}}

    turnNumDenomsToBitmap(numDenoms.toMap)
  }

  def turnNumDenomsToBitmap(numDenoms: Map[(Int, Int), (Float, Float, Float)]): Unit = {
    //val mapImage = new BufferedImage(cols, rows, BufferedImage.TYPE_4BYTE_ABGR)
    //val uniformAlphaMapImage = new BufferedImage(cols, rows, BufferedImage.TYPE_4BYTE_ABGR)
    val certaintyOpacityDivergingImage = new BufferedImage(cols, rows, BufferedImage.TYPE_4BYTE_ABGR)
    val singleHueBins = Seq((255,245,235),(254,230,206),(253,208,162),(253,174,107),(253,141,60),(241,105,19),(217,72,1), (166,54,3),(127,39,4),(97,19,4))
    val divergingHueBins = Seq((5,48,97),(33,102,172),(67,147,195),(146,197,222),(209,229,240),(253,219,199),(244,165,130),(214,96,77),(178,24,43),(103,0,31))

    val grayMedianHueBins = Seq((49, 22, 202), (78, 58, 190), (107, 94, 178), (136, 130, 166), (166, 166, 155), (172, 124, 116), (178, 83, 77), (184, 41, 38), (191, 0, 0))


    val colors = singleHueBins.map(s => new Color(s._1, s._2, s._3, 127).getRGB)
    val kitsCertainty = 2

    numDenoms.foreach{case((col: Int, row: Int), (num: Float, denom: Float, certaintyWeight: Float)) => {
      val intensPercent = num / denom
      val intensity = Math.min((intensPercent * 255).toInt, 255)
      val certaintyPercent = certaintyWeight / kitsCertainty
      val certaintyOpacity = Math.min((certaintyPercent * 255).toInt, 255)
      val thisColor = new Color(255, 255, 0, intensity)
      //mapImage.setRGB(col, rows - row - 1, thisColor.getRGB)
      val hueBinIndex = Math.min((intensPercent * grayMedianHueBins.size).toInt, grayMedianHueBins.size - 1)
      //uniformAlphaMapImage.setRGB(col, rows - row - 1, colors(hueBinIndex))
      val divergingHueBinColor = new Color(grayMedianHueBins(hueBinIndex)._1, grayMedianHueBins(hueBinIndex)._2, grayMedianHueBins(hueBinIndex)._3, certaintyOpacity)
      certaintyOpacityDivergingImage.setRGB(col, rows - row - 1, divergingHueBinColor.getRGB)
    }}
    //ImageIO.write(mapImage, "PNG", new File("target\\Z631vsL283alphaChannel.png"))
    //ImageIO.write(uniformAlphaMapImage, "PNG", new File("target\\Z631vsL283colorBands.png"))
    ImageIO.write(certaintyOpacityDivergingImage, "PNG", new File("target\\" + signal + " vs non-" + signal + " " + signalParent  + " gray.png"))
  }

  def createThreeSignalBitmap(signalOne: String, signalTwo: String) {
    if (geometries.keySet.size == 0) loadGeometries()

    /*val countryNumDenoms = Map[String, (Int, Int)]("Germany" -> (0,10),
                                                   "France" -> (1,1))
*/
    val cellSignals = mutable.HashMap[(Int, Int), (Float, Float, Float, Float)]()

    def addToExistingCellSignals(col: Int, row: Int, weight: Float, signals: Array[Int], certaintyWeight: Float) {
      cellSignals.get(col, row) match {
        case Some(x) => cellSignals.put((col, row), (x._1 + weight * signals(0),
          x._2 + weight * signals(1),
          x._3 + weight * signals(2),
          x._4 + certaintyWeight))
        case _ => cellSignals.put((col, row), (weight * signals(0),
          weight * signals(1),
          weight * signals(2),
          certaintyWeight))
      }
    }

    def processCountry(countryName: String, signals: Array[Int]): Unit = {
      println("processing " + countryName + " into grid cells")
      if (countryCellWeights.get(countryName).isEmpty) {
        val (gridWeights, certaintyWeight) = turnCountryIntoGridWeights(europeBounds, countryName)
        countryCellWeights.put(countryName, gridWeights)
        countryCertaintyWeight.put(countryName, certaintyWeight)
      }
      val cWeight = countryCertaintyWeight.get(countryName).get * signals.sum
      countryCellWeights.get(countryName).get.foreach{case((col: Int, row: Int), weight: Float) => addToExistingCellSignals(col, row, weight, signals, cWeight)
      }
    }
    println("parsing CSV")
    val (countrySignals, pointSignals) = parseCSVTwoSignals(signalOne, signalTwo)

    countrySignals.foreach{case((countryName: String, signals: Array[Int])) => processCountry(countryName, signals)}

    /*val pointNumerators = Seq[(Float, Float, Boolean)]((48.333F, 11.566F, true))*/

    pointSignals.foreach{case((lat: Float, lon: Float, index: Int)) => {
      turnLatLonIntoGridWeights(europeBounds, lat, lon, 320F).foreach{case((col: Int, row: Int), (weight: Float, certaintyWeight: Float)) => {
        if (worldValidity.contains((col, row))) {
          val dummy = Array[Int](0,0,0)
          dummy(index) = 1
          addToExistingCellSignals(col, row, weight, dummy, certaintyWeight)
        }
      }}}}

    turnSignalsToBitmap(signalOne, signalTwo, cellSignals.toMap)
  }

  def turnSignalsToBitmap(signalOne: String, signalTwo: String, cellSignals: Map[(Int, Int), (Float, Float, Float, Float)]): Unit = {
    val certaintyOpacityDivergingImage = new BufferedImage(cols, rows, BufferedImage.TYPE_4BYTE_ABGR)
    val red = (229,0,7) //0
    val blue = (0, 10, 191) //1
    val yellow = (238, 227, 27) //2
    val colorLookup = mutable.Map[(Int, Int),(Seq[(Int, Int, Int)])]()
    val redToBlue = Seq((219, 0, 118), (200, 0, 210), (90, 0, 200))
    val yellowToRed = Seq((235, 171, 20), (233, 113, 13), (231, 53, 6))
    val yellowToBlue = Seq((164, 232, 23), (15, 220, 30), (8, 208, 173))

    colorLookup.put((0, 1), redToBlue)
    colorLookup.put((1, 0), redToBlue.reverse)
    colorLookup.put((0, 2), yellowToRed.reverse)
    colorLookup.put((2, 0), yellowToRed)
    colorLookup.put((1, 2), yellowToBlue.reverse)
    colorLookup.put((2, 1), yellowToBlue)
    val pureColors = Seq(red, blue, yellow)

    val kitsCertainty = 2

    cellSignals.foreach{case((col: Int, row: Int), (signalOne: Float, signalTwo: Float, signalThree: Float, certaintyWeight: Float)) => {

      val ((bestIndex, bestValue), (secondIndex, secondValue)) = getTopTwoIndices(signalOne, signalTwo, signalThree)

      val color = (bestValue > secondValue * 2, bestValue > secondValue * 4) match {
        case (true, true) => pureColors(bestIndex)
        case (true, false) => (colorLookup.get((bestIndex, secondIndex)).get)(0)
        case (false, false) => (colorLookup.get((bestIndex, secondIndex)).get)(1)

      }

      val certaintyPercent = certaintyWeight / kitsCertainty
      val certaintyOpacity = Math.min((certaintyPercent * 255).toInt, 255)
      val thisColor = new Color(color._1, color._2, color._3, certaintyOpacity)
      certaintyOpacityDivergingImage.setRGB(col, rows - row - 1, thisColor.getRGB)
    }}
    //ImageIO.write(mapImage, "PNG", new File("target\\Z631vsL283alphaChannel.png"))
    //ImageIO.write(uniformAlphaMapImage, "PNG", new File("target\\Z631vsL283colorBands.png"))
    ImageIO.write(certaintyOpacityDivergingImage, "PNG", new File("target\\" + signalOne + " vs " + signalTwo + " and yellow basal.png"))
  }

  def getTopTwoIndices(signalOne: Float, signalTwo: Float, signalThree: Float): ((Int, Float), (Int, Float)) = {
    val best = Seq((0, signalOne), (1, signalTwo), (2, signalThree)).sortBy(p => p._2).reverse.take(2)
    (best(0), best(1))
  }
}
